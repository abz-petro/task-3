FROM ubuntu:18.04

USER root

# Need for tzdata
ENV DEBIAN_FRONTEND=noninteractive

# Update all packages
RUN apt-get update

# Tools
RUN apt-get install -yq --no-install-recommends \
    git \
    apt-utils \
    curl \
    openssl \
    nano \
    graphicsmagick \
    iputils-ping \
    ca-certificates

# Nginx
RUN apt-get install -yq --no-install-recommends nginx

# PHP 7.3
RUN apt-get install software-properties-common -y && add-apt-repository ppa:ondrej/php
RUN apt-get install -yq --no-install-recommends \
    php7.3 \
    php7.3-fpm \
    php7.3-json \
    php7.3-curl \
    php7.3-mbstring \
    php7.3-mysql \
    php7.3-xml \
    php7.3-zip \
    mysql-client
# PHP EXTS
RUN apt-get install -yq --no-install-recommends \
    php7.3-gd \
    php7.3-bcmath

# Install composer (only with php)
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

WORKDIR /var/www

# set polices
RUN chown -R www-data:www-data /var/www/

CMD service php7.3-fpm start ; nginx -g 'daemon off;' ;
